import React from "react";
import "../sass/design.scss";
import { Form, Button, Table } from "react-bootstrap";

import Imgtitle from "../img/title.png";
const teamregister = () => {
  return (
    <div
      className="body_unitregister"
      style={{
        backgroundImage: `url(${require("../img/valorantpage/Valorant.png")})`,
        backgroundPosition: "center",
        backgroundSize: "100% 100%",
        backgroundRepeat: "no-repeat",
      }}
    >
      <div className="grid-container1-tr">
        <div className="grid-item1-tr">
          <img src={Imgtitle} alt="imgtitle" />
        </div>
      </div>
      <div className="grid-container2-tr">
        <div className="grid-item2-tr">
          <h4>
            Porfavor la primera parte del formulario, ingrese los datos del
            lider del equipo:
          </h4>
        </div>
      </div>
      <div className="grid-container3-tr">
        <div className="grid-item3-tr">
          <Form.Control className="nombresinput" placeholder="Nombres" />
          <Form.Control className="apellidosinput" placeholder="Apellidos" />
        </div>
      </div>
      <div className="grid-container4-tr">
        <div className="grid-item4-tr">
          <Form.Control className="direccioninput" placeholder="Direccion" />
        </div>
      </div>
      <div className="grid-container5-tr">
        <div className="grid-item5-tr">
          <Form.Control
            className="emailinput"
            type="email"
            placeholder="Correo Electronico"
          />
          <Form.Control
            className="telinput"
            type="phone"
            maxLength="8"
            placeholder="Telefono"
          />
          <Form.Control className="zipinput" placeholder="Zip Code" />
        </div>
      </div>
      <div className="grid-container6-tr">
        <div className="grid-item6-tr">
          <Form.Control className="departamentoinput" as="select">
            <option>Alta Verapaz</option>
            <option>Baja Verapaz</option>
            <option>Chimaltenango</option>
            <option>Chiquimula</option>
            <option>El Progreso</option>
            <option>Escuintla</option>
            <option>Guatemala</option>
            <option>Huehuetenango</option>
            <option>Izabal</option>
            <option>Jalapa</option>
            <option>Jutiapa</option>
            <option>Petén</option>
            <option>Quetzaltenango</option>
            <option>Quiché</option>
            <option>Retalhuleu</option>
            <option>Sacatepéquez</option>
            <option>San Marcos</option>
            <option>Santa Rosa</option>
            <option>Sololá</option>
            <option>Suchitepéquez</option>
            <option>Totonicapán</option>
            <option>Zacapa</option>
          </Form.Control>
          <Form.Control className="ciudadinput" placeholder="Ciudad" />
          <Form.Control
            className="userinput"
            placeholder="Tu usuario o ID en el juego -> (Nombre#xxxx)"
          />
        </div>
      </div>
      <div className="grid-container7-tr">
        <div className="grid-item7-tr">
          <h4>Ingrese la informacion del resto del equipo:</h4>
          <Table striped bordered hover variant="dark" className="tabla-tr">
            <thead>
              <tr>
                <th>Id de Usuario</th>
                <th>Personaje</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <Form.Control className="" placeholder="Id #1" />
                </td>
                <td>
                  <Form.Control className="departamentoinput" as="select">
                    <option>Phoenix</option>
                    <option>Jett</option>
                    <option>Brimstone</option>
                    <option>Cypher</option>
                    <option>Omen</option>
                    <option>Reyna</option>
                    <option>Raze</option>
                    <option>Sage</option>
                    <option>Sova</option>
                    <option>Viper</option>
                    <option>Breach</option>
                  </Form.Control>
                </td>
                <td></td>
              </tr>
              <tr>
                <td>
                  <Form.Control className="" placeholder="Id #2" />
                </td>
                <td>
                  <Form.Control className="departamentoinput" as="select">
                    <option>Phoenix</option>
                    <option>Jett</option>
                    <option>Brimstone</option>
                    <option>Cypher</option>
                    <option>Omen</option>
                    <option>Reyna</option>
                    <option>Raze</option>
                    <option>Sage</option>
                    <option>Sova</option>
                    <option>Viper</option>
                    <option>Breach</option>
                  </Form.Control>
                </td>
                <td></td>
              </tr>
              <tr>
                <td>
                  <Form.Control className="" placeholder="Id #3" />
                </td>
                <td>
                  <Form.Control className="departamentoinput" as="select">
                    <option>Phoenix</option>
                    <option>Jett</option>
                    <option>Brimstone</option>
                    <option>Cypher</option>
                    <option>Omen</option>
                    <option>Reyna</option>
                    <option>Raze</option>
                    <option>Sage</option>
                    <option>Sova</option>
                    <option>Viper</option>
                    <option>Breach</option>
                  </Form.Control>
                </td>
                <td></td>
              </tr>
              <tr>
                <td>
                  <Form.Control className="" placeholder="Id #4" />
                </td>
                <td>
                  <Form.Control className="departamentoinput" as="select">
                    <option>Phoenix</option>
                    <option>Jett</option>
                    <option>Brimstone</option>
                    <option>Cypher</option>
                    <option>Omen</option>
                    <option>Reyna</option>
                    <option>Raze</option>
                    <option>Sage</option>
                    <option>Sova</option>
                    <option>Viper</option>
                    <option>Breach</option>
                  </Form.Control>
                </td>
                <td></td>
              </tr>
              <tr>
                <td>
                  <Form.Control className="" placeholder="Id #5" />
                </td>
                <td>
                  <Form.Control className="departamentoinput" as="select">
                    <option>Phoenix</option>
                    <option>Jett</option>
                    <option>Brimstone</option>
                    <option>Cypher</option>
                    <option>Omen</option>
                    <option>Reyna</option>
                    <option>Raze</option>
                    <option>Sage</option>
                    <option>Sova</option>
                    <option>Viper</option>
                    <option>Breach</option>
                  </Form.Control>
                </td>
                <td></td>
              </tr>
            </tbody>
          </Table>
        </div>
      </div>
      <div className="grid-container8-tr">
        <div className="grid-item8-tr">
          <Button className="registrabtn" variant="success">
            Registrarse
          </Button>{" "}
        </div>
      </div>
    </div>
  );
};

export default teamregister;
