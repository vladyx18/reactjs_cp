//rafce
import React from "react";
import "../sass/design.scss";
import Cp from "../img/mainpage/toppage_features.png";
import Cp1 from "../img/mainpage/competitive-pool_centerpage.png";
import Cp2 from "../img/mainpage/valorant_gameselect.png";
import Cp3 from "../img/mainpage/warzone_gameselect.png";
import Cp4 from "../img/mainpage/fifa20_gameselect.png";
import Cp5 from "../img/mainpage/lol_gameselect.png";

const main = () => {
  return (
    <div
      className="body-main"
      style={{
        backgroundImage: `url(${require("../img/mainpage/Delicate.jpg")})`,
        backgroundPosition: "center",
        backgroundSize: "100% 100%",
        backgroundRepeat: "no-repeat",
      }}
    >
      <div className="grid-container1-main">
        <div className="grid-item1-main">
          <img src={Cp} className="topfeatures" alt="" />
        </div>
      </div>
      <div className="grid-container2-main">
        <div className="grid-item2-main">
          <img src={Cp1} className="midtitle" alt="" />
        </div>
      </div>
      <div className="grid-container3-main">
        <div className="grid-item3-main">
          <img src={Cp2} className="valorant_gameselect" alt="" />
          <img src={Cp3} className="warzone_gameselect" alt="" />
          <img src={Cp4} className="fifa20_gameselect" alt="" />
          <img src={Cp5} className="lol_gameselect" alt="" />
        </div>
      </div>
    </div>
  );
};

export default main;
