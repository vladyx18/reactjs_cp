import React from "react";
import "../sass/design.scss";
import { Form, Button, Table } from "react-bootstrap";

import Imgtitle from "../img/title.png";
import Phoenix from "../img/valorantcards/phoenix.jpg";
import Jett from "../img/valorantcards/jett.jpg";
import Omen from "../img/valorantcards/omen.jpg";
import Raze from "../img/valorantcards/raze.jpg";
import Reyna from "../img/valorantcards/reyna.jpg";
import Sage from "../img/valorantcards/sage.jpg";
import Viper from "../img/valorantcards/viper.jpg";
import Breach from "../img/valorantcards/breach.jpg";
import Brimstone from "../img/valorantcards/brimstone.jpg";
import Cypher from "../img/valorantcards/cypher.jpg";
import Sova from "../img/valorantcards/sova.jpg";

const unitregister = () => {
  return (
    <div
      className="body_unitregister"
      style={{
        backgroundImage: `url(${require("../img/valorantpage/Valorant.png")})`,
        backgroundPosition: "center",
        backgroundSize: "100% 100%",
        backgroundRepeat: "no-repeat",
      }}
    >
      <div className="grid-container1-ur">
        <div className="grid-item1-ur">
          <img src={Imgtitle} alt="imgtitle" />
        </div>
      </div>
      <div className="grid-container2-ur">
        <div className="grid-item2-ur">
          <Form.Control className="nombresinput" placeholder="Nombres" />
          <Form.Control className="apellidosinput" placeholder="Apellidos" />
        </div>
      </div>
      <div className="grid-container3-ur">
        <div className="grid-item3-ur">
          <Form.Control className="direccioninput" placeholder="Direccion" />
        </div>
      </div>
      <div className="grid-container4-ur">
        <div className="grid-item4-ur">
          <Form.Control
            className="emailinput"
            type="email"
            placeholder="Correo Electronico"
          />
          <Form.Control
            className="telinput"
            type="phone"
            maxLength="8"
            placeholder="Telefono"
          />
          <Form.Control className="zipinput" placeholder="Zip Code" />
        </div>
      </div>
      <div className="grid-container5-ur">
        <div className="grid-item5-ur">
          <Form.Control className="departamentoinput" as="select">
            <option>Alta Verapaz</option>
            <option>Baja Verapaz</option>
            <option>Chimaltenango</option>
            <option>Chiquimula</option>
            <option>El Progreso</option>
            <option>Escuintla</option>
            <option>Guatemala</option>
            <option>Huehuetenango</option>
            <option>Izabal</option>
            <option>Jalapa</option>
            <option>Jutiapa</option>
            <option>Petén</option>
            <option>Quetzaltenango</option>
            <option>Quiché</option>
            <option>Retalhuleu</option>
            <option>Sacatepéquez</option>
            <option>San Marcos</option>
            <option>Santa Rosa</option>
            <option>Sololá</option>
            <option>Suchitepéquez</option>
            <option>Totonicapán</option>
            <option>Zacapa</option>
          </Form.Control>
          <Form.Control className="ciudadinput" placeholder="Ciudad" />
          <Form.Control
            className="userinput"
            placeholder="Tu usuario o ID en el juego -> (Nombre#xxxx)"
          />
        </div>
      </div>
      <div className="grid-container6-ur">
        <div className="grid-item6-ur">
          <Table striped bordered hover variant="dark">
            <thead>
              <tr>
                <th>Personaje</th>
                <th>Nombre del Personaje</th>
                <th>Selección</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <img className="avatar" src={Phoenix} alt="" />
                </td>
                <td>
                  <h3 className="titlecentertable">Phoenix</h3>
                </td>
                <td>
                  <Form.Check
                    type="switch"
                    id="custom-switch"
                    label="‎ ‏‏‎"
                    className="centerswitch"
                  />
                </td>
              </tr>
              <tr>
                <td>
                  <img className="avatar" src={Jett} alt="" />
                </td>
                <td>
                  <h3 className="titlecentertable">Jett</h3>
                </td>
                <td>
                  <Form.Check
                    type="switch"
                    id="custom-switch2"
                    label="‎ ‏‏‎"
                    className="centerswitch"
                  />
                </td>
              </tr>
              <tr>
                <td>
                  <img className="avatar" src={Brimstone} alt="" />
                </td>
                <td>
                  <h3 className="titlecentertable">Brimstone</h3>
                </td>
                <td>
                  <Form.Check
                    type="switch"
                    id="custom-switch3"
                    label="‎ ‏‏‎"
                    className="centerswitch"
                  />
                </td>
              </tr>
              <tr>
                <td>
                  <img className="avatar" src={Cypher} alt="" />
                </td>
                <td>
                  <h3 className="titlecentertable">Cypher</h3>
                </td>
                <td>
                  <Form.Check
                    type="switch"
                    id="custom-switch4"
                    label="‎ ‏‏‎"
                    className="centerswitch"
                  />
                </td>
              </tr>
              <tr>
                <td>
                  <img className="avatar" src={Omen} alt="" />
                </td>
                <td>
                  <h3 className="titlecentertable">Omen</h3>
                </td>
                <td>
                  <Form.Check
                    type="switch"
                    id="custom-switch5"
                    label="‎ ‏‏‎"
                    className="centerswitch"
                  />
                </td>
              </tr>
              <tr>
                <td>
                  <img className="avatar" src={Reyna} alt="" />
                </td>
                <td>
                  <h3 className="titlecentertable">Reyna</h3>
                </td>
                <td>
                  <Form.Check
                    type="switch"
                    id="custom-switch6"
                    label="‎ ‏‏‎"
                    className="centerswitch"
                  />
                </td>
              </tr>
              <tr>
                <td>
                  <img className="avatar" src={Raze} alt="" />
                </td>
                <td>
                  <h3 className="titlecentertable">Raze</h3>
                </td>
                <td>
                  <Form.Check
                    type="switch"
                    id="custom-switch7"
                    label="‎ ‏‏‎"
                    className="centerswitch"
                  />
                </td>
              </tr>
              <tr>
                <td>
                  <img className="avatar" src={Sage} alt="" />
                </td>
                <td>
                  <h3 className="titlecentertable">Sage</h3>
                </td>
                <td>
                  <Form.Check
                    type="switch"
                    id="custom-switch8"
                    label="‎ ‏‏‎"
                    className="centerswitch"
                  />
                </td>
              </tr>
              <tr>
                <td>
                  <img className="avatar" src={Sova} alt="" />
                </td>
                <td>
                  <h3 className="titlecentertable">Sova</h3>
                </td>
                <td>
                  <Form.Check
                    type="switch"
                    id="custom-switch9"
                    label="‎ ‏‏‎"
                    className="centerswitch"
                  />
                </td>
              </tr>
              <tr>
                <td>
                  <img className="avatar" src={Viper} alt="" />
                </td>
                <td>
                  <h3 className="titlecentertable">Viper</h3>
                </td>
                <td>
                  <Form.Check
                    type="switch"
                    id="custom-switch10"
                    label="‎ ‏‏‎"
                    className="centerswitch"
                  />
                </td>
              </tr>
              <tr>
                <td>
                  <img className="avatar" src={Breach} alt="" />
                </td>
                <td>
                  <h3>Breach</h3>
                </td>
                <td>
                  <Form.Check
                    type="switch"
                    id="custom-switch11"
                    label="‎ ‏‏‎"
                    className="centerswitch"
                  />
                </td>
              </tr>
            </tbody>
          </Table>
        </div>
      </div>
      <div className="grid-container7-ur">
        <div className="grid-item7-ur">
          <Button className="registrabtn" variant="success">
            Registrarse
          </Button>{" "}
        </div>
      </div>
    </div>
  );
};

export default unitregister;
