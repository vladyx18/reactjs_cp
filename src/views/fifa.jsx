//rafce
import React from "react";
import "../sass/design.scss";
import { Table, Button } from "react-bootstrap";

import Titleimg from "../img/title.png";

const lolpage = () => {
  return (
    <div
      className="body-v"
      style={{
        backgroundImage: `url(${require("../img/fifapage/fifa.png")})`,
        backgroundPosition: "center",
        backgroundSize: "100% 83%",
        backgroundRepeat: "no-repeat",
      }}
    >
      <div className="grid-container1-v">
        <div className="grid-item1-v">
          <img src={Titleimg} alt="titleimg" />
        </div>
      </div>
      <div className="grid-container2-v">
        <div className="grid-item2-v"></div>
      </div>
      <div className="grid-container3-v">
        <div className="grid-item3-v">
          <Table striped bordered hover variant="dark">
            <thead>
              <tr>
                <th>#</th>
                <th>Descripcion</th>
                <th>Premio</th>
                <th>Cupo</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>Torneo Fifa 1vs1</td>
                <td>$50.00</td>
                <td>19/20</td>
                <td>
                  <Button variant="outline-success">Registrarse</Button>{" "}
                </td>
              </tr>
              <tr>
                <td>2</td>
                <td>Torneo Fifa Ultimateam</td>
                <td>$25.00</td>
                <td>15/22</td>
                <td>
                  <Button variant="outline-success">Registrarse</Button>{" "}
                </td>
              </tr>
              <tr>
                <td>3</td>
                <td>Torneo Equipos 2vs2</td>
                <td>$50.00</td>
                <td>11/16</td>
                <td>
                  <Button variant="outline-success">Registrarse</Button>{" "}
                </td>
              </tr>
              <tr>
                <td>4</td>
                <td>Tonrneo sin reglas 1vs1</td>
                <td>$35.00</td>
                <td>20/20</td>
                <td>
                  <Button variant="outline-danger">Cerrado</Button>{" "}
                </td>
              </tr>
            </tbody>
          </Table>
        </div>
      </div>
      <div className="grid-container4-v">
        <div className="grid-item4-v">
          <Button variant="warning">Pagina Principal</Button>{" "}
        </div>
      </div>
    </div>
  );
};

export default lolpage;
