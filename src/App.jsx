import React from "react";
import "./App.css";

import Pagina from "./views/lolpage.jsx";

function App() {
  return (
    <div className="App">
      <Pagina />
    </div>
  );
}

export default App;
